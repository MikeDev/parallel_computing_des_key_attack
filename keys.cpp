//
// Created by mikedev on 24/07/18.
//

#include "keys.h"

/**
 * Get the i-th key as string
 * @param i: the index of the i-th key
 * @param key_container a char array that will contains the string key.
 * For DES encryption the size should be 10
 * @return the pointer of key_container
 */
char* get_key(int i, char *key_container){
    std::sprintf(key_container, "%08i", i);
    return key_container;
}


RandomGenerator::RandomGenerator(int a, int b) : a(a), b(b) {
    std::random_device rd;
    std::uniform_int_distribution<int>unif (a, b);
    this->p_gen = new std::mt19937(rd());
    this->p_unif = new std::uniform_int_distribution<int> (a, b);
}

int RandomGenerator::operator()(){
    return (*(this->p_unif))(*this->p_gen);
}

RandomGenerator::~RandomGenerator() {
    delete this->p_gen;
    delete this->p_unif;
};