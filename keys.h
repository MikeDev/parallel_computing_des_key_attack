//
// Created by mikedev on 24/07/18.
//

#include <cmath>
#include <random>
#include <functional>

#ifndef DES_BRUTE_PC_KEYS_H
#define DES_BRUTE_PC_KEYS_H

    const int max_value_key = 100000;
    const int num_password_generated = 10000;
    const int first_year = 0;
    char* get_key(int i, char* key_container);

class RandomGenerator{
        public:
            const int a;
            const int b;
            RandomGenerator(int a, int b);
            ~RandomGenerator();
            int operator()();
        private:
            std::uniform_int_distribution<int> *p_unif;
            std::mt19937 *p_gen;
};


struct AttackResult{
    int pass;
    int turns;
    long time;
};


struct TaskGroup{
    const int start;
    const int end;
    const std::string cypherkey;
    int *key;
    int *iterations;
    const std::string salt;
};


struct Key{
    int pass;
    std::array<char, 2> salt;
};



#endif //DES_BRUTE_PC_KEYS_H
