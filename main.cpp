#include <crypt.h>
#include <sstream>
#include <fstream>
#include <random>
#include <cstring>
#include <iostream>
#include "parallel_brute_attack.h"
#include "keys.h"

template<class Data>
void save_results(const std::string &filename, const std::vector<Data> times) {
    std::stringstream relpath;
    relpath << "../results/" << filename ;
    std::stringstream text_to_write;
    for(auto time: times){
        text_to_write << time << std::endl;
    }
    std::ofstream file;
    file.open(relpath.str(), std::ios_base::app | std::ios_base::out);
    file << text_to_write.str();
    file.close();
}

std::pair<std::string, std::string> pass_to_hack(){
    auto generator = RandomGenerator(0, num_password_generated - 1);
    auto line = generator();
    std::cout << "Read password of line " << line << std::endl;
    std::string filename = "../passwords.txt";
    std::ifstream file;
    std::string pass_line;
    file.open(filename, std::ios_base::in);
    for(auto i = 0; i < line; i++){
        std::getline(file, pass_line);
    }
    std::string crypt_pass(pass_line.substr(0, 13));
    std::string salt(pass_line.substr(14, 2));
    std::string original_password(pass_line.substr(17, pass_line.length()));
    std::cout << "original password: " << original_password << std::endl;
    return {crypt_pass, salt};
};


int main(int argc, char **argv) {
    if(argc != 3){
        std::cerr << "Input arguments must be 2: {num_threads} {num_executions}";
        return 1;
    }
    auto num_threads = atoi(argv[1]);
    auto num_executions = atoi(argv[2]);
    std::vector<double> times(num_executions);
    std::vector<int> iterations(num_executions);
    auto parallel_attack = ParallelBruteAttack(num_threads, max_value_key, get_key);
    for(auto i = 0; i < num_executions; i++){
        auto pass_and_salt = pass_to_hack();
        auto encr_key = pass_and_salt.first;
        auto salt = pass_and_salt.second;
        auto attack_result = parallel_attack.run(encr_key, salt);
        std::cout << "Found key by brute force attack: " << attack_result.pass << std::endl;
        auto second_time =  static_cast<double>(attack_result.time) / 1000 / 1000;
        times[i] = second_time;
        iterations[i] = attack_result.turns;
    }
    std::stringstream times_file, iteration_file;
    times_file << "attack_times_" << num_threads << ".txt";
    iteration_file << "attack_iterations_" << num_threads << ".txt";
    save_results(times_file.str(), times);
    save_results(iteration_file.str(), iterations);
    return 0;
}

