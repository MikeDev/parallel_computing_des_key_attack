//
// Created by mikedev on 21/09/18.
//

#include "parallel_brute_attack.h"


ParallelBruteAttack::ParallelBruteAttack(int n_thread, int num_keys, std::function<void(int, char*)> key_generator)
        : n_threads(n_thread), num_keys(num_keys), key_generator(std::move(key_generator)) {}

AttackResult ParallelBruteAttack::run(const std::string &cypherkey,
                                   const std::string &salt) {
    this->init_attack_params(cypherkey, salt);
    auto time = this->measure_attack_time();
    auto key = *this->tasks->at(0).key;
    auto niters = *this->tasks->at(0).iterations;
    delete this->tasks->at(0).key;
    delete this->tasks->at(0).iterations;
    delete this->tasks;
    return AttackResult{key, niters, time};
}

std::vector<std::pair<int, int>>* ParallelBruteAttack::create_ranges() {
    auto keys_per_thread = static_cast<int>(floor(this->num_keys / this->n_threads));
    std::vector<int> key_threads(this->n_threads, keys_per_thread);
    key_threads[key_threads.size() - 1] += this->num_keys % this->n_threads;
    int cumsum = 0;
    auto ranges = new std::vector<std::pair<int, int>>(0);
    for(int thread = 0; thread < this->n_threads; thread++){
        auto start = cumsum; auto end = cumsum+key_threads[thread];
        ranges -> emplace_back(start, end);
        cumsum = end;
    }
    return ranges;
}

long ParallelBruteAttack::measure_attack_time() {
    auto init = std::chrono::_V2::system_clock::now();
    std::vector<std::thread> threads;
    for(const TaskGroup &task: *this->tasks){
        auto thread = std::thread(ParallelBruteAttack::thread_attack, task, this->key_generator);
        threads.push_back(std::move(thread));
    }
    for(std::thread &thread: threads){
        thread.join();
    }
    auto end = std::chrono::_V2::system_clock::now();
    auto exec_time = end - init;
    long mcr_exec_time = std::chrono::duration_cast<std::chrono::microseconds>(exec_time).count();
    return mcr_exec_time;
}

std::vector<TaskGroup> *ParallelBruteAttack::create_tasks(std::vector<std::pair<int, int>> *ranges,
                                                  const std::string &cypherkey,
                                                  const std::string &salt) {
    auto key = new int(-1);
    auto niterations = new int(-1);
    auto tasks = new std::vector<TaskGroup>();
    for(auto range: *ranges){
        const int start = range.first;
        const int end = range.second;
        auto taskg = TaskGroup{start, end, cypherkey, key, niterations, salt};
        tasks->push_back((taskg));
    }
    return tasks;
}


void ParallelBruteAttack::init_attack_params(const std::string &cypherkey,
                                          const std::string &salt) {
    this->ranges = this->create_ranges();
    this->tasks = this->create_tasks(this->ranges, cypherkey, salt);
    delete this->ranges;
}

void ParallelBruteAttack::thread_attack(const TaskGroup &task,
                                     std::function<void(int, char *)> key_generator) {
    auto k = task.start;
    auto data = new crypt_data;
    auto str_pass = new char[10];
    for(; k < task.end && *task.key == -1; k++){
        key_generator(k, str_pass);
        data->initialized = 0;
        std::string gen_cypherkey(crypt_r(str_pass, task.salt.c_str(), data));
        if(gen_cypherkey == task.cypherkey){
            *task.key = k;
            *task.iterations = k - task.start;
            delete data;
            delete[] str_pass;
            return;
        }
    }
    delete[] str_pass;
    delete data;

}


