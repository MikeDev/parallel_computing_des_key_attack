//
// Created by mikedev on 21/09/18.
//

#ifndef DES_BRUTE_PC_ATTACKTHREAD_H
#define DES_BRUTE_PC_ATTACKTHREAD_H

#include <functional>
#include <vector>
#include <chrono>
#include <thread>
#include <crypt.h>
#include "keys.h"

class ParallelBruteAttack {
    public:
        ParallelBruteAttack(int n_thread, int num_keys,
                         std::function<void(int, char*)>key_generator);
        AttackResult run(const std::string &cypherkey,
                         const std::string &salt);
    private:
        int n_threads;
        int num_keys;
        std::vector<std::pair<int, int>>* ranges;
        std::vector<TaskGroup>* tasks;
        std::function<void(int, char*)> key_generator;
        std::vector<std::pair<int, int>>* create_ranges();
        std::vector<TaskGroup> *create_tasks(std::vector<std::pair<int, int>> *ranges,
                                             const std::string &cypherkey,
                                             const std::string &salt);
        static void thread_attack(const TaskGroup &task, std::function<void(int, char *)> key_generator);
        void init_attack_params(const std::string &cypherkey,
                                 const std::string &salt);
        long measure_attack_time();

};




#endif //DES_BRUTE_PC_ATTACKTHREAD_H
