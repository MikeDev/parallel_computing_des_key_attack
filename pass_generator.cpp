//
// Created by mikedev on 17/07/18.
//
#include <random>
#include <crypt.h>
#include <fstream>
#include <array>
#include <iostream>
#include "keys.h"

const char lowercase_letters[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                                  'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};


std::array<char, 2> random_salt(){
    auto random_generator = RandomGenerator(0, 25);
    int i1 = random_generator();
    int i2 = random_generator();
    return {lowercase_letters[i1], lowercase_letters[i2]};
}


void save_to_file(int num_keys, std::string *crypt_keys, std::array<char, 2>* salts, int *keys){
    auto filename = "../passwords.txt";
    std::ofstream file;
    file.open(filename, std::ios_base::trunc);
    for(auto i = 0; i < num_keys; i++){
        auto strkey = new char[10];
        get_key(keys[i], strkey);
        file << crypt_keys[i] << " " << salts[i][0] << salts[i][1] << " " << strkey << std::endl;
        delete[] strkey;
    }
    file.close();
}


int main(int argv, char **argc){
    int keys[num_password_generated];
    auto random_gen = RandomGenerator(0, max_value_key);
    std::string crypt_keys[num_password_generated];
    std::array<char, 2> salts[num_password_generated];
    auto strkey = new char[10];
    for(auto i = 0; i < num_password_generated; i++){
        int random_num = random_gen();
        keys[i] = random_num;
        get_key(random_num, strkey);
        auto salt = random_salt();
        crypt_keys[i] = std::string(crypt(strkey, salt.data()));
        salts[i] = salt;
    }
    save_to_file(num_password_generated, crypt_keys, salts, keys);
}