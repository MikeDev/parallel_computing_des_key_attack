import seaborn as sns
import matplotlib.pyplot as plt
from path import Path
import pandas as pd
import sys; sys.setrecursionlimit(20000)
import numpy as np
DATA = Path(__file__).parent.parent / 'results'

THREADS = [1, 2, 3, 4, 5, 6, 7, 8]
PLOTS_FOLDER = Path(__file__).parent

def thread_diff():
    files = [(i, f'attack_times_{i}.txt') for i in THREADS]
    df = pd.DataFrame()
    times = []
    n_thread_list = []
    for n_threads, file in files:
        with open(DATA / file) as f:
            lines = f.readlines()
            times += list(map(float, lines))
            n_thread_list += [n_threads] * len(lines)
    df['num_threads'] = n_thread_list
    df['times'] = times
    ax = df.boxplot(column='times', by='num_threads')
    fig = ax.get_figure()
    fig.suptitle('')
    ax.set_ylabel('Seconds')
    ax.set_title('Times by num threads')
    fig.savefig('plots/times_by_num_threads.png')
    plt.close()
    return df



def plot_and_save_speedups(df):
    df = df.groupby('num_threads').mean()
    speedups = []
    for i in THREADS:
        speedup = df.loc[1, 'times'] / df.loc[i, 'times']
        speedups.append(speedup)
    sns.barplot(x=THREADS, y=speedups)
    plt.grid(axis='y')
    plt.title('Speedup to single thread execution')
    plt.ylabel('Speedup')
    plt.xlabel('# Threads')
    plt.savefig(f'plots/speedup.png')
    plt.close()
    df.to_csv(f'results/speedup.csv')

def save_keys_per_thread():
    x = THREADS
    y = [43896 / i for i in x]
    plt.plot(x, y, '-o')
    plt.ylabel('keys per thread')
    plt.xlabel('Num threads')
    plt.title('Keys per thread when num thread increase')
    plt.savefig('plots/keys_per_thread.png')


def plot_variance(df):
    var_times = df.groupby('num_threads').std()
    ax = sns.barplot('num_threads', 'times', data=df, ci=None)
    ax.set_title('Standard deviation of times by num thread')
    ax.set_ylabel('Std of times')
    plt.savefig('plots/var_times_per_thread.png')    

def plot_time_distribution(df: pd.DataFrame):
    plt.close()
    for nthreads in THREADS:
        times_ = df.query(f'num_threads == {nthreads}')['times']
        sns.kdeplot(times_)
    plt.legend([f'{t} Thread' for t in THREADS])
    plt.savefig(PLOTS_FOLDER / f'kde_threads.png')
    plt.close()

def main():
    # plot_difference_early_stop()
    t_diff = thread_diff()
    # save_keys_per_thread()
    plot_and_save_speedups(t_diff)
    plot_variance(t_diff)
    plot_time_distribution(t_diff)


if __name__ == '__main__':
    main()
