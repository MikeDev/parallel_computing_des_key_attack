\contentsline {section}{\numberline {1}\hskip -1em.\nobreakspace {}Introduzione all'attacco}{1}{section.1}% 
\contentsline {section}{\numberline {2}\hskip -1em.\nobreakspace {}Password e cifratura}{1}{section.2}% 
\contentsline {section}{\numberline {3}\hskip -1em.\nobreakspace {}Specifiche tecniche}{1}{section.3}% 
\contentsline {section}{\numberline {4}\hskip -1em.\nobreakspace {}Strumenti usati durante lo sviluppo del programma}{1}{section.4}% 
\contentsline {section}{\numberline {5}\hskip -1em.\nobreakspace {}Come compilare il progetto}{1}{section.5}% 
\contentsline {section}{\numberline {6}\hskip -1em.\nobreakspace {}Spiegazione del progetto}{2}{section.6}% 
\contentsline {subsection}{\numberline {6.1}\hskip -1em.\nobreakspace {}Password possibili}{2}{subsection.6.1}% 
\contentsline {subsection}{\numberline {6.2}\hskip -1em.\nobreakspace {}Parallelismo}{2}{subsection.6.2}% 
\contentsline {subsection}{\numberline {6.3}\hskip -1em.\nobreakspace {}Terminazione della ricerca}{2}{subsection.6.3}% 
\contentsline {subsection}{\numberline {6.4}\hskip -1em.\nobreakspace {}Eseguibili e compilazione}{2}{subsection.6.4}% 
\contentsline {section}{\numberline {7}\hskip -1em.\nobreakspace {}Presentazione dei risultati e commento}{2}{section.7}% 
\contentsline {section}{\numberline {8}\hskip -1em.\nobreakspace {}Appendice}{3}{section.8}% 
\contentsline {subsection}{\numberline {8.1}\hskip -1em.\nobreakspace {}Tabella tempi per thread}{3}{subsection.8.1}% 
